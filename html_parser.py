import re
from bs4 import BeautifulSoup


class HtmlParser:
    def __init__(self, data):
        self.data = data
    
    # Add div tag if the text is not wrapped in a div tag so bs4 can find its text later
    def add_div_tags(self):
        # if self.data.startswith("<div>") and self.data.endswith("</div>"):
        # if beginning.search(self.data) and ending.search(self.data):
        #     return self.data
        # else:
        return "<div>" + self.data + "</div>"
    
    def remove_div_tag(self, text):
        modified_text = text
        # Find the first occurrence of "<div>" 
        first_index = text.find("<div>")
        if first_index != -1:
            modified_text = text[first_index + 5:]
        # Find the last occurrence of "</div>"
        last_index = modified_text.rfind("</div>")
        if last_index != -1:
            modified_text = modified_text[:last_index]
        return modified_text

    
    # Remove formatting tags
    def remove_format_tag(self):
        # all_format_tag = ['abbr', 'address', 'b', 'bdi', 'bdo', 'blockquote', 'cite', 'code', 'del', 'dfn', 'em', 'i', 'ins', 'kbd', 'mark', 'meter', 'pre', 'progress', 'q', 'rp', 'rt', 'ruby', 's', 'samp', 'small', 'strong', 'sub', 'sup', 'template', 'time', 'u', 'var', 'wbr', 'span']

        # Format tags to be removed
        format_tag = ['span', 'abbr', 'b', 'bdi', 'bdo', 'cite', 'code', 'dfn', 'em', 'i', 'kbd', 'mark', 'pre', 'small', 'strong', 'time', 'var', 'wbr']

        # Parse the HTML content with BeautifulSoup
        prep_html = self.add_div_tags()
        self.soup = BeautifulSoup(prep_html, 'html.parser')

        # Find and store all HTML tags in an array (list)
        all_tags = self.soup.find_all()

        # Iterate through all HTML tags
        for tag in all_tags:
            # Get all sub tags as array
            tag_list = tag.contents
            for i, tag_comp in enumerate(tag_list):
                if tag_comp.name in format_tag:
                    tag_list[i].replace_with(tag_comp.text)
                else:
                    continue

        # Remove unnescessary spaces
        self.soup = re.sub(' +', ' ', str(self.soup))
        return self.soup
    
    # Get all text content as arrays in array
    def get_text_content(self):
        # Remove format tags
        html = self.remove_format_tag()
        # Parse bs and find and store all HTML tags in an array (list)
        self.soup = BeautifulSoup(html, 'html.parser')
        all_tags = self.soup.find_all()
        # Container to store text
        container = []
        index = 0
        # Iterate through all HTML tags
        for tag in all_tags:
            # Smaller container to store sub texts in each tag
            container.append([])
            # Get all sub tags and texts content
            tag_list = tag.contents
            # Iterate through contents
            for i, tag_comp in enumerate(tag_list):
                # Find texts in html tag
                if isinstance(tag_comp, str) and tag_comp != '' and tag_comp != '\n':
                    container[index].append(tag_list[i])
                else:
                    # Otherwise add empty string to pertain position
                    container[index].append('')
            index += 1
        return container
    
    
    # Get translation back to html
    def get_result(self, translated: list):
        all_tags = self.soup.find_all()
        index = 0
        # Iterate through all HTML tags
        for tag in all_tags:
            # Get sub tags and texts
            tag_list = tag.contents
            # Iterate through contents
            for i, tag_comp in enumerate(tag_list):
                # Find text contents
                if isinstance(tag_comp, str) and tag_comp != '' and tag_comp != '\n':
                    tag_list[i].replace_with(translated[index][i])
                else:
                    continue
            index += 1
        return self.remove_div_tag(self.soup.prettify())
    
    def __str__(self):
        return self.get_result()
